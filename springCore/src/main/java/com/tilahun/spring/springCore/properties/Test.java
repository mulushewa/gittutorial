package com.tilahun.spring.springCore.properties;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext properties = new ClassPathXmlApplicationContext(
				"com/tilahun/spring/springCore/properties/propertyconfig.xml");

		CountriesAndLanguages property = (CountriesAndLanguages) properties.getBean("countriesAndLang");
		System.out.println(property);
	}

}
