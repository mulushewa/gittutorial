package com.tilahun.spring.springCore.refTpyes;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext ref = new ClassPathXmlApplicationContext(
				"com/tilahun/spring/springCore/refTpyes/refTypeconfig.xml");

		Student student = (Student) ref.getBean("student");
		System.out.println(student);
	}

}
