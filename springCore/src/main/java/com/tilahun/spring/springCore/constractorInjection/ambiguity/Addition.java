package com.tilahun.spring.springCore.constractorInjection.ambiguity;

public class Addition {

	Addition(double a, double b) {
		System.out.println("Inside Constractor DOUBLE");
	}

	Addition(int a, int b) {
		System.out.println("Inside Constractor INT");
	}

	Addition(String a, String b) {
		System.out.println("Inside Constractor String");
	}

}
