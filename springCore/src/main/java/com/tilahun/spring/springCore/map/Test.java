package com.tilahun.spring.springCore.map;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext map = new ClassPathXmlApplicationContext(
				"com/tilahun/spring/springCore/map/mapconfig.xml");

		Object customer = map.getBean("customer");
		System.out.println(customer.toString());
	}

}
