package com.tilahun.spring.springCore.set;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {

		ClassPathXmlApplicationContext set = new ClassPathXmlApplicationContext(
				"com/tilahun/spring/springCore/set/setconfig.xml");

		CarDealer car = (CarDealer) set.getBean("cardealer");
		System.out.println(car.getName());
		System.out.println(car.getModels().getClass());
	}

}
