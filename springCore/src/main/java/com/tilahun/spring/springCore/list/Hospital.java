package com.tilahun.spring.springCore.list;

import java.util.List;

public class Hospital {

	private String name;
	private List<String> departmensts;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getDepartmensts() {
		return departmensts;
	}

	public void setDepartmensts(List<String> departmensts) {
		this.departmensts = departmensts;
	}

}
